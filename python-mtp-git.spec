Name: python-mtp-git
Version: v1.0.r4.g1ab37bb
Release: 1
Summary: 'Automated/interactive cryptanalysis for the Many-time pad attack'
License: GPL3
Requires: python-urwid
Provides: python-mtp-git
BuildRequires: git
#Source0: python-mtp::git+https://github.com/CameronLonsdale/MTP.git
%define __brp_mangle_shebangs %{nil}
%define debug_package %{nil}
%define srcdir %{_builddir}
%define pkgname python-mtp-git

%prep
 

%description
'Automated/interactive cryptanalysis for the Many-time pad attack'

%build
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    cd "$srcdir/${pkgname%-git}";
    sed -i 's/urwid==2.0.1/urwid/g' setup.py;
    python setup.py build

%install
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    cd "$srcdir/${pkgname%-git}";
    python setup.py install --root="$pkgdir/" --optimize=1 --skip-build

%files
/*
%exclude %dir /usr/bin
%exclude %dir /usr/lib

